<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Liste des utilisateurs";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}




$profil = profilagent($_SESSION['user']);






function tableau($qualif_profil){


	// le var_assoc permet de sortir le tableau en order by la variable sur la requete connect_table_sans_assoc.
	$var_assoc = 'id';

	if (isset($_GET['sort'])) {

		if ($_GET['sort'] == 'id')
		{
			$var_assoc = 'id';
		}
		elseif ($_GET['sort'] == 'nom')
		{
			$var_assoc = 'nom';
		}
		elseif ($_GET['sort'] == 'preom')
		{
			$var_assoc = 'preom';
		}
		elseif($_GET['sort'] == 'ddn')
		{
			$var_assoc = 'ddn';
		}
		elseif($_GET['sort'] == 'mail')
		{
			$var_assoc = 'mail';
		}
		elseif($_GET['sort'] == 'grade')
		{
			$var_assoc = 'grade';
		}
		elseif($_GET['sort'] == 'qualif')
		{
			$var_assoc = 'qualif';
		}
		elseif($_GET['sort'] == 'profil')
		{
			$var_assoc = 'profil';
		}
		elseif($_GET['sort'] == 'service')
		{
			$var_assoc = 'service';
		}

	}



	// var_assoc permet le order by.
	$un_agent = connect_table_sans_assoc('agent',$var_assoc);

	// tableau : entetes : 

	echo '<table class="pure-table">
	<thead>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=id">Id<a/></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=nom">Nom</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=prenom">Prenom</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=ddn">Date de naissance</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=mail">Email</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=periode">Grade</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=qualif">Qualification</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=profil">Profil</a></th>
	<th class="thead_tableau transition"><a href="liste_agents.php?sort=service">Service</a></th>';

	if ($qualif_profil == 4) {
		echo '<th style="color: rgb(243, 156, 18);">Modifier</th>';
		echo '<th  style="color: #D73C2C;">Supprimer</th>';
	}

	echo '</thead>
	<tbody>';

	foreach ($un_agent as $key) {
		echo "<tr>";

		// affiche la date de début au format français
		if ($key['ddn'] != '') {
			$ddn = $key['ddn'] ;
			$datedebutex = explode('-', $ddn);
			$ddn = $datedebutex[2] . ' / ' . $datedebutex[1] . ' / ' . $datedebutex[0] ;
		}
		else {
			$ddn = '';
		}



		// ON affiche la qualification depuis la table correspondante

		if($key['qualif'] != NULL){
			$verif_qualif  = $key['qualif'];
		}
		else {
			$verif_qualif = '';
		}

		if($verif_qualif != ''){
			$qualif = connect_table_where('qualification','id',$verif_qualif);
		}
		else {
			$qualif = connect_table('qualification');
		}


		if ($qualif[0]['lib'] == '') {
			$qualification_agent = '_';
		}
		else {
			$qualification_agent = $qualif[0]['lib'];
		}



		// ON affiche le grade depuis la table correspondante

		if($key['grade'] != NULL){
			$verif_grade  = $key['grade'];

		}
		else {
			$verif_grade = '';
		}

		if($verif_grade != ''){
			$grade = connect_table_where('grade','id',$verif_grade);
		}
		else {
			$grade = connect_table('grade');
		}


		if ($grade[0]['lib'] == '') {
			$grade_agent = '_';
		}
		else {
			$grade_agent = $grade[0]['lib'];
		}

		//


		// on affiche le profil depuis la table correspondante

		if($key['profil'] != NULL){
			$verif_profil  = $key['profil'];


		}
		else {
			$verif_profil = '';
		}

		if($verif_profil != ''){
			$profil = connect_table_where('profil','id',$verif_grade);
		}
		else {
			$profil = connect_table('profil');
		}


		if ($profil[0]['lib'] == '') {
			$profil_agent = '_';
		}
		else {
			$profil_agent = $profil[0]['lib'];
		}

		//


		// on affiche le service depuis la table correspondante

		if($key['service'] != NULL){
			$verif_service  = $key['service'];


		}
		else {
			$verif_service = '';
		}

		if($verif_service != ''){
			$service = connect_table_where('service','id',$verif_service);
		}
		else {
			$service = connect_table('service');
		}

// on teste si un service est attribué à l'agent
		if(!isset($service[0])){
			
				$service_agent = '_';
			
		}
		else {
			$service_agent = $service[0]['lib'];
		}

		


		//


		if ($key['mail'] == '') {
			$key['mail'] = 'inconnu';
		}



		// on met un élément en brut par colonne.

		echo "<td>" . $key['id'] . "</td>";
		echo "<td>" . $key['nom'] . "</td>";
		echo "<td>" . $key['prenom'] . "</td>";
		echo "<td>" . $ddn . "</td>";
		echo "<td>" . $key['mail'] . "</td>";
		echo "<td>" . $grade_agent . "</td>";
		echo "<td>" .  $qualification_agent . "</td>";
		echo "<td>" . $profil_agent . "</td>";
		echo "<td>" . $service_agent . "</td>";
		if ($qualif_profil == 4) {
			echo '<td><a href="modifier_user.php?id='.$key['id'].'">Modifier</a></td>';
			echo '<td><a href="suppr_user.php?id='.$key['id'].'">Supprimer</a></td>';
		}
		echo '</tr>';

	}

	echo "</tbody></table>";

}


?>



<?php


switch ($profil) {
	// si on est sur un profil direction
	case 1:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);
		# code...
		break;
	// si on est sur un profil chef de service
	case 2:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);
		break;
	// si on est sur un profil agent
	case 3:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);



		break;
	// si on est sur un profil administrateur (secrétaire)
	case 4:
		echo "<p>Formation disponibles :  </p>";
		echo tableau($profil);
		break;

	default:
		echo "Erreur";
		break;
}

?>


	<br />




<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>