<?php


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Crypter les mots de passe";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if ($profil != 4) {
	header('Location: compte.php');

}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$var_assoc = 'id';

	// var_assoc permet le order by.
	$agent = connect_table_sans_assoc('agent',$var_assoc);

	foreach ($agent as $key) {
		$mdp_cryp = crypt($key[5], 'dgfip_63');


		$stmt = $la_connexion->prepare("UPDATE agent SET mdp = :mdp
					WHERE id = :id");

		$stmt->bindParam(':mdp', $mdp_cryp);
		$stmt->bindParam(':id', $key[0]);
		$stmt->execute();
	}


	header('Location: crypt_mdp.php');


}

function tableau($qualif_profil){


	// le var_assoc permet de sortir le tableau en order by la variable sur la requete connect_table_sans_assoc.
	$var_assoc = 'id';

	if (isset($_GET['sort'])) {

		if ($_GET['sort'] == 'id')
		{
			$var_assoc = 'id';
		}
		elseif ($_GET['sort'] == 'agent_nom')
		{
			$var_assoc = 'agent_nom';
		}
		elseif ($_GET['sort'] == 'agent_prenom')
		{
			$var_assoc = 'agent_prenom';
		}
		elseif($_GET['sort'] == 'agent_mdp')
		{
			$var_assoc = 'agent_mdp';
		}
		elseif($_GET['sort'] == 'agent_mdp_crypt')
		{
			$var_assoc = 'agent_mdp_crypt';
		}

	}



	// var_assoc permet le order by.
	$agent = connect_table_sans_assoc('agent',$var_assoc);

	// tableau : entetes : 

	echo '<table class="pure-table">
	<thead>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=id">Id_agent<a/></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=agent_nom">Nom</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=agent_prenom">Prenom</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=agent_mdp">Mdp</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=agent_mdp_crypt">Mdp crypté : </a></th>';


	echo '</thead>
	<tbody>';

	foreach ($agent as $key) {
		echo "<tr>";

		// affiche la date de début au format français
		if ($key[0] != '') {
			$datedebut = $key[1] ;
		}
		else {
			$datedebut = '';
		}

		$mdp_cryp = crypt($key[5], 'dgfip_63');
		// on met un élément en brut par colonne.

		echo "<td>" . $key[0] . "</td>";
		echo "<td>" . $key[2] . "</td>";
		echo "<td>" . $key[3] . "</td>";
		echo "<td>" . $key[5] . "</td>";
		echo "<td>" . $mdp_cryp . "</td>";
		echo '</tr>';
	}

	echo "</tbody></table>";

}



echo "<p>Voici les formations disponibles: </p>";
echo tableau($profil);



// LE CONTENU :
?>





	<form class="pure-form pure-form-aligned" method="post">


		<button type="submit1" class="pure-button pure-button-primary">Envoyer</button>
	</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>