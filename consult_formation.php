<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Consultation des formations";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}




$profil = profilagent($_SESSION['user']);



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_POST['code']) && $_POST['code']){

	}
	else if(isset($_POST['agent']) && $_POST['agent']) {

	}

}


if(isset($_GET['date'])){
	$date = $_GET['date'];
	$iframe =  '<iframe src="./calendrier/?annee='.$date.'" width="100%" height="700px"  frameBorder="0"></iframe>';
}
else {
	$iframe = '<iframe src="./calendrier/" width="100%" height="700px"  frameBorder="0"></iframe>';
}



function tableau($qualif_profil){


	// le var_assoc permet de sortir le tableau en order by la variable sur la requete connect_table_sans_assoc.
	$var_assoc = 'id';

	if (isset($_GET['sort'])) {

		if ($_GET['sort'] == 'id')
		{
			$var_assoc = 'id';
		}
		elseif ($_GET['sort'] == 'code')
		{
			$var_assoc = 'type_formation';
		}
		elseif ($_GET['sort'] == 'nom')
		{
			$var_assoc = 'nom_formation';
		}
		elseif($_GET['sort'] == 'date_debut')
		{
			$var_assoc = 'date_debut';
		}
		elseif($_GET['sort'] == 'date_fin')
		{
			$var_assoc = 'date_fin';
		}
		elseif($_GET['sort'] == 'periode')
		{
			$var_assoc = 'periode';
		}
		elseif($_GET['sort'] == 'ville')
		{
			$var_assoc = 'ville';
		}
		elseif($_GET['sort'] == 'fournisseur')
		{
			$var_assoc = 'id_fournisseur';
		}

	}



	// var_assoc permet le order by.
	$formation = connect_table_sans_assoc('formation',$var_assoc);

	// tableau : entetes : 

	echo '<table class="pure-table">
	<thead>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=id">Id<a/></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=code">Code</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=nom">Nom</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=date_debut">Date de début</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=date_fin">Date de fin</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=periode">Période</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=ville">Zone géographique</a></th>
	<th class="thead_tableau transition"><a href="consult_formation.php?sort=ville">Fournisseur</a></th>';

	if ($qualif_profil == 4) {
		echo '<th style="color: rgb(243, 156, 18);">Modifier</th>';
		echo '<th  style="color: #D73C2C;">Supprimer</th>';
	}

	echo '</thead>
	<tbody>';

	foreach ($formation as $key) {
		echo "<tr>";

		// affiche la date de début au format français
		if ($key[3] != '') {
			$datedebut = $key[3] ;
			$datedebutex = explode('-', $datedebut);
			$datedebut = $datedebutex[2] . ' / ' . $datedebutex[1] . ' / ' . $datedebutex[0] ;
		}
		else {
			$datedebut = '';
		}


		// affiche la date de fin au format français

		if ($key[4] != '') {
			$datefin = $key[4] ;
			$datefinex = explode('-', $datefin);
			$datefin = $datefinex[2] . ' / ' . $datefinex[1] . ' / ' . $datefinex[0] ;
		}
		else {
			$datefin = '';
		}




		$nom_fournisseur = connect_table_where('fournisseur','id',$key[8]);

		// si jamais on tombe sur une formation sans id fournisseur.
		if(!isset($nom_fournisseur[0])){
			$nom_fournisseur[0]['lib'] = '?';
		}
		else {
			// oui c'est spaghetti mais bon ...
			$nom_fournisseur[0]['lib'] = $nom_fournisseur[0]['lib'];
		}


		// on met un élément en brut par colonne.

		echo "<td>" . $key[0] . "</td>";
		echo "<td>" . $key[1] . "</td>";
		echo "<td>" . $key[2] . "</td>";
		echo "<td>" . $datedebut . "</td>";
		echo "<td>" . $datefin . "</td>";
		echo "<td>" . $key[9] . "</td>";
		echo "<td>" . $key[7] . "</td>";
		echo "<td>" .  $nom_fournisseur[0]['lib'] . "</td>";
		// si profil admin
		if ($qualif_profil == 4) {
			echo '<td><a href="modifier_formation.php?id='.$key[0].'">Modifier</a></td>';
			echo '<td><a href="suppr_formation.php?id='.$key[0].'">Supprimer</a></td>';
		}
		echo '</tr>';
	}

	echo "</tbody></table>";

}


?>



<?php


switch ($profil) {
	// si on est sur un profil direction
	case 1:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);
		# code...
		break;
	// si on est sur un profil chef de service
	case 2:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);
		break;
	// si on est sur un profil agent
	case 3:
		echo "<p>Voici les formations disponibles: </p>";
		echo tableau($profil);



		break;
	// si on est sur un profil administrateur (secrétaire)
	case 4:
		echo "<p>Voici les formations disponibles :  </p>";
		echo tableau($profil);
		break;

	default:
		echo "Erreur";
		break;
}

?>


	<br />



	<form class="pure-form pure-form-aligned" action="<?php  echo $_SERVER['PHP_SELF']; ?>" method="get">


		<fieldset>

			<div class="pure-control-group"> <label for="date">Changer d'année</label>

				<select name="date" id="date">
					<?php


					for ($i=2000; $i < 2100; $i++) {
						echo '<option value="' . $i . '">' . $i. '</option>';
					}

					?>
				</select> </div>


			<button type="submit1" class="pure-button pure-button-primary">Envoyer</button>


		</fieldset>

	</form>


<?php

echo $iframe;
// LE PIED DE PAGE
require_once 'inc/footer.php';
?>