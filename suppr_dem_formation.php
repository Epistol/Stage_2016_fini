<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Suppresion de demande de formation ...";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if (isset($_GET['id_demande'] )) {

	$nom_formation = connect_table_where('demande_formation','id',$_GET['id_demande']);


	$id_form = $_GET['id_demande'];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isset($_POST['submit'])) {

			$stmt = $la_connexion->prepare("DELETE FROM demande_formation WHERE id = :id");

			$stmt->bindParam(':id', $id_form);
			$stmt->execute();
			header('Location: consult_demande_formation.php');


		}
		else if(isset($_POST['non'])){
			header('Location: consult_demande_formation.php');
		}
	}


}



?>

	<form class="pure-form pure-form-aligned" method="post">

		<div class="pure-controls">
			<p>Êtes-vous sur de vouloir supprimer la demande de formation " <b><?php echo $nom_formation[0]['id']; ?> </b>"?</p>

			<button type="submit" name="submit" class="pure-button pure-button-primary">Oui</button>
			<button type="submit" name="non" class="pure-button pure-button-primary">Non</button>
		</div>
	</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>