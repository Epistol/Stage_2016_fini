<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Compte";
//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';




if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}


$profil = profilagent($_SESSION['user']);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if (isset($_POST['psd1'])) {
		$psd = htmlspecialchars($_POST['psd1']);
	}
	else {
		echo "N'oubliez pas de mettre un mot de passe dans le premier champ ...";

	}

	if (isset($_POST['psd2'])) {
		$psd2 = htmlspecialchars($_POST['psd2']);

	}
	else {
		echo 'Noubliez pas de retaper votre mot de passe ...';

	}

	if($psd == $psd2){

		$mdp_cryp = crypt($psd2, 'dgfip_63');


		$sql = "UPDATE agent SET mdp = :mdp  
			WHERE id = :id";
		$stmt = $la_connexion->prepare($sql);
		$stmt->bindParam(':mdp',$mdp_cryp, PDO::PARAM_STR);
		$stmt->bindParam(':id', $_SESSION['user'], PDO::PARAM_STR);
		$stmt->execute();


		header('Location: compte.php');
	}


	else {
		echo 'Les mots de passes ne sont pas les même ...';

	}


}



?>


<form class="pure-form pure-form-stacked" method="post"> <fieldset> <legend>Changement de mot de passe </legend> <label for="psd1">Mot de passe 1</label> <input id="psd1" type="password" name="psd1" placeholder="Nouveau mot de passe"> <label for="psd2">Tapez le une seconde fois</label> <input id="psd2" name="psd2" type="password" placeholder="Password"> <button type="submit" class="pure-button pure-button-primary">Changer</button> </fieldset> </form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>
