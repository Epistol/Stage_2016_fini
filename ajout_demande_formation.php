<?php


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Ajout formation";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';

?>


	<h1 style="text-align: center;">Fiche de candidature à une action de formation 155-SD</h1>


	<form class="pure-form pure-form-aligned" action="partie2.php" method="post">
		<fieldset>
			<legend>Candidat</legend>
			<div class="pure-control-group"> <label for="name">Agent</label>


				<select name="agent" id="agent">
					<?php

					$agent = connect_table('agent');


					foreach ($agent as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['nom']). ' ' . ucfirst($row['prenom']) . '</option>';

					} ?>
					<option value="other">Autre</option>
				</select>
			</div>



		</fieldset>





		<button type="submit" class="pure-button pure-button-primary">Envoyer</button>
	</form>

<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>