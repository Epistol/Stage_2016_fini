<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Modifier une formation";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';

// si l'utilisateur n'est 
if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}



// si on est pas sur un profil administrateur, on redirige la personne sur son compte
if ($profil != 4) {
	header('Location: compte.php');

}



$formation = connect_table_sans_assoc('formation');
$nom_fournisseur = connect_table('fournisseur');

// si on a envoyé le formulaire
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$codeformation = htmlspecialchars($_POST['code_forma']);
	$nomforma = htmlspecialchars($_POST['nom_forma']);

	$datedebut = $_POST['datedebut'];
	$datefin = $_POST['datefin'];



	// on récupère chaque champ, on échappe les caractères spéciaux avec htmlspecialchars (éviter d'avoir SELECT * dans un <input> par exemple)
	if (isset($_POST['adresse'])) {
		$adresse = htmlspecialchars($_POST['adresse']);
	}
	if (isset($_POST['zone'])) {
		$zone = htmlspecialchars($_POST['zone']);
	}
	if (isset($_POST['cp'])) {
		$cp = htmlspecialchars($_POST['cp']);
	}
	if (isset($_POST['fournisseur'])) {
		$fournisseur = htmlspecialchars($_POST['fournisseur']);

	}

	// si les deux champs obligatoires sont bien saisis : (vérification simple)
	if (isset($codeformation) && isset($nomforma)) {



		$stmt = $la_connexion->prepare("UPDATE formation SET type_formation = :type_formation, nom_formation = :nom_formation,  date_debut = :date_debut, date_fin = :date_fin, adresse = :adresse, code_postal = :code_postal, ville = :ville, id_fournisseur = :id_fournisseur, periode  = :periode
				WHERE id = :id");

		$stmt->bindParam(':type_formation', $codeformation);
		$stmt->bindParam(':nom_formation', $nomforma);
		$stmt->bindParam(':date_debut', $datedebut);
		$stmt->bindParam(':date_fin', $datefin);
		$stmt->bindParam(':adresse', $adresse);
		$stmt->bindParam(':code_postal', $cp);
		$stmt->bindParam(':ville', $zone);
		$stmt->bindParam(':id_fournisseur', $fournisseur);
		$stmt->bindParam(':periode', $periode);
		$stmt->bindParam(':id', $_GET['id']);
		$stmt->execute();

		echo "L'entrée à bien été modifiée";
		header('Location: consult_formation.php');
		exit();


	}

}






?>




	<h2><?php

		// le formulaire :

		// on affiche le nom de la page

		echo $varpage . ' : ' . $_GET['id'];

		$formation = connect_table_where('formation','id',$_GET['id']);

		?>


	</h2>


	<form class="pure-form pure-form-aligned" method="post">
		<fieldset>
			<div class="pure-control-group">
				<label for="code_forma">Code formation</label>
				<input id="code_forma" name="code_forma" type="text" placeholder="" value="<?php echo $formation[0]['type_formation']; ?>" required>
			</div>
			<div class="pure-control-group">
				<label for="nom_forma">Nom de la formation</label>
				<input id="nom_forma" name="nom_forma" type="text" placeholder="" value="<?php echo $formation[0]['nom_formation']; ?>" required>
			</div>
			<div class="pure-control-group">
				<label for="datedebut">Date de début : </label>
				<input name="datedebut" id="datedebut" class="dateTxt"  value="<?php echo $formation[0]['date_debut']; ?>"  type="text" >
			</div>
			<div class="pure-control-group">
				<label for="datefin">Date de fin : </label>
				<input id="datefin" name="datefin" class="dateTxt" type="text" value="<?php echo $formation[0]['date_fin']; ?>" >
			</div>
			<div class="pure-control-group">
				<label for="adresse">Adresse</label>
				<input id="adresse" name="adresse" type="text" placeholder="" value="<?php echo $formation[0]['adresse']; ?>">
			</div>
			<div class="pure-control-group">
				<label for="zone">Ville ou Zone géographique</label>
				<input id="zone" name="zone" type="text" placeholder="" value="<?php echo $formation[0]['ville']; ?>">
			</div>
			<div class="pure-control-group">
				<label for="cp">Code Postal</label>
				<input id="cp" name="cp" type="text" placeholder="" value="<?php echo $formation[0]['code_postal']; ?>">
			</div>
			<div class="pure-control-group">
				<label for="fournisseur">Fournisseur</label>



				<select name="fournisseur" id="fournisseur">
					<?php


					foreach ($nom_fournisseur as $row){


						if($row['id'] == $formation[0]['id_fournisseur']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}
						echo '<option ' . $var_select . ' value="' . $row['id'] . '">' . ucfirst($row['lib']) . '</option>';

					} ?>
					<option value="other">Autre</option>
				</select>

			</div>

			<div class="pure-control-group">
				<label for="periode">Période</label>
				<input id="periode" name="periode" type="text" value="<?php echo $formation[0]['periode']; ?>">
			</div>


			<div class="pure-controls">

				<button type="submit" name="submit" class="pure-button pure-button-primary">Submit</button>
			</div>
		</fieldset>
	</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>