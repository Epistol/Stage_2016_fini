<?php


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Enregistrer un utilisateur";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if ($_SERVER['REQUEST_METHOD'] == 'POST') {



	if(isset($_POST['nom'])){
		if (!empty($_POST['nom'])) {
			$nom =  strtolower(htmlspecialchars($_POST['nom']));

		}
	}
	if(isset($_POST['service'])){
		if (!empty($_POST['service'])) {
			$service =  htmlspecialchars($_POST['service']);

		}
	}
	if(isset($_POST['qualification'])){
		if (!empty($_POST['qualification'])) {
			$qualification =  htmlspecialchars($_POST['qualification']);

		}
	}
	if(isset($_POST['profil'])){
		if (!empty($_POST['profil'])) {
			$profil =  htmlspecialchars($_POST['profil']);

		}
	}
	if(isset($_POST['grade'])){
		if (!empty($_POST['grade'])) {
			$grade =  htmlspecialchars($_POST['grade']);

		}
	}
	if(isset($_POST['dgfipid'])){
		if (!empty($_POST['dgfipid'])) {
			$dgfipid =  htmlspecialchars($_POST['dgfipid']);

		}
	}

	if(isset($_POST['ddn'])){

		$ddn =  htmlspecialchars($_POST['ddn']);


	}
	if(isset($_POST['prenom'])){
		if (!empty($_POST['prenom'])) {
			$prenom = strtolower(htmlspecialchars($_POST['prenom']));
		}
	}
	if(isset($_POST['password'])){
		if (!empty($_POST['password'])) {
			$mdp_pre = htmlspecialchars($_POST['password']);


			$mdp = crypt($mdp_pre, 'dgfip_63');


		}
	}

	$mail_agent = $prenom.'.'.$nom.'@dgfip.finances.gouv.fr';



	$stmt = $la_connexion->prepare("INSERT INTO agent (id,ddn,nom,prenom,mail,mdp,grade, qualif,profil, service) VALUES (:id,:ddn,:nom, :prenom,:mail, :mdp,:grade,:qualif,:profil, :service)");
	$stmt->bindParam(':id', $dgfipid);
	$stmt->bindParam(':ddn', $ddn);
	$stmt->bindParam(':mail', $mail_agent);
	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':mdp', $mdp);
	$stmt->bindParam(':grade', $grade);
	$stmt->bindParam(':qualif', $qualification);
	$stmt->bindParam(':profil', $profil);
	$stmt->bindParam(':service', $service);
	$stmt->execute();

}


// LE CONTENU :
?>




	<p>Enregistrer un nouvel agent : </p>

	<form method="post" class="pure-form pure-form-aligned" action="<?php $_SERVER['PHP_SELF']?>">
		<fieldset>
			<div class="pure-control-group"> <label for="nom">ID DGFiP: </label> <input name ="dgfipid" id="dgfipid" type="text" required placeholder=""> </div>
			<div class="pure-control-group"> <label for="nom">Nom : </label> <input name ="nom" id="nom" type="text" required placeholder=""> </div>
			<div class="pure-control-group"> <label for="prenom">Prenom : </label> <input name ="prenom" id="prenom" type="text" required placeholder=""> </div>
			<div class="pure-control-group"> <label for="password">Mot de passe : </label> <input id="password" name="password" type="password" required placeholder="">
			</div>
			<div class="pure-control-group"> <label for="ddn">Date de naissance: </label> <input name ="ddn" id="ddn" type="text" required placeholder=""> </div>

			<div class="pure-control-group">
				<label for="service">Service : </label>
				<select name="service" id="service">
					<?php

					$agent = connect_table('service');


					foreach ($agent as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="qualification">Qualification : </label>
				<select name="qualification" id="qualification">
					<?php

					$agent = connect_table('qualification');


					foreach ($agent as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="grade">Grade : </label>
				<select name="grade" id="grade">
					<?php

					$grade = connect_table('grade');


					foreach ($grade as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="profil">Profil : </label>
				<select name="profil" id="profil">
					<?php

					$profil_t = connect_table('profil');


					foreach ($profil_t as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>


			<button type="submit" class="pure-button pure-button-primary">Envoyer</button>
		</fieldset>
	</form>



<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>