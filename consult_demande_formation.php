<?php

// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Consultation des formations";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}



$id_util = $_SESSION['user'];





// La fonction tableau prend en paramètre la qualification de l'utilisateur (ou le champ 'profil' depuis la table agent dans la bdd)
// elle prend aussi l'id de l'utilisateur qui consulte le tableau, qui sera $id_util déclaré plus haut

function tableau($qualif_profil, $id_util2){



	// le var_assoc permet de sortir le tableau en order by la variable sur la requete connect_table_sans_assoc.
	$var_assoc = 'id';




	// var_assoc permet le order by.

	// si c'est un profil 'agent', il verra ses propres demandes de formation
	if ($qualif_profil == 3) {
		$formation = connect_table_where_order_by('demande_formation','id_agent',$id_util2,$var_assoc);

	}

	// si c'est un profil 'chef de service'
	elseif ($qualif_profil == 2) {
		$agent = returnagent($id_util2);
		$rq = 'SELECT * FROM demande_formation WHERE id_agent IN ( SELECT id from agent WHERE service = ' .  $agent['service']. ')';
		$formation = connect_table_rq_orderby($rq,  $var_assoc  );
	}
	// si c'est un profil 'administrateur'
	else if ($qualif_profil == 4) {
		$rq = 'SELECT * FROM demande_formation';
		$formation = connect_table_rq_orderby($rq,  $var_assoc  );
	}




	// tableau : entetes : 



	echo '<table class="pure-table" id="report">
	<thead>
	<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=id">Numéro<a/></th>

	';

	if ($qualif_profil != 3) {
		echo '<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=etat">Etat de formation<a/></th>';
		echo '<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=employe">Employé<a/></th>';


	}


	echo '
	<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=nom">Nom</a></th>
		<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=dtae_demande_agent">Date de demande</a></th>
	<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=periode">Période</a></th>
	<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=ville">Zone géographique</a></th>
	<th class="thead_tableau transition"><a href="consult_demande_formation.php?sort=fournisseur">Fournisseur</a></th>';


	echo '</thead>
	<tbody>';
	$i = 1;

	foreach ($formation as $key) {


		if ($key[2] != NULL ){
			$nom_de_formation = connect_table_where_sans_fetch('formation','id',$key[2]);
			$nomfor = $nom_de_formation->fetch();
		}
		else {
			$nomfor = 'Inconnu';
		}



		$date_de_demande_de_lagent = $key['dtae_demande_agent'];
		$date_de_demande_de_lagentr = sort_date_fr($date_de_demande_de_lagent);

		$employe = connect_table_where_sans_fetch('agent','id',$key[1]);
		$emplye_rq = $employe->fetch();


		if(!empty($key['date_choix_debut_1'])){
			$date_debut = sort_date_fr($key['date_choix_debut_1']);
		}
		else {
			$date_debut = sort_date_fr($key['date_choix_debut_2']);
		}
		if(!empty($key['date_choix_fin_1'])){
			$date_fin = sort_date_fr($key['date_choix_fin_1']);
		}
		else {
			$date_fin = sort_date_fr($key['date_choix_fin_1']);
		}

		if(!empty($key['lieux_1'])){
			$lieux = $key['lieux_1'];
		}
		else {
			$lieux =$key['lieux_2'];
		}



		if ($key['id_fournisseur'] != ''){
			$id_fournisseur = connect_table_where_sans_fetch('fournisseur','id',$key['id_fournisseur']);
			$fournisseur = $id_fournisseur->fetch();
		}
		else {
			$fournisseur = '?';
		}


		if ($key['status_formation'] != ''){
			switch ($key['status_formation']) {
				case 1:
					$couleur = 'background:#4caf50';
					$status = 'Validé';
					break;
				case 2:
					$couleur = 'background:#f44336';
					$status = 'Annulé';
					break;
				case 3:
					$couleur = 'background:#ff9800';
					$status = 'Reporté';
					break;
				default:
					$status = 'Pas de statut';
					break;
			}
		}
		else {
			$status = 'Pas de statut';
		}





		echo "<tr>";


		// on met un élément en brut par colonne.

		echo "<td>" .$i . "</td>";


		if ($qualif_profil != 3 ) {
			echo '<td style=" ' . $couleur .' ">' . ucfirst($status)  . '</td>';
			echo '<td>' . ucfirst($emplye_rq['nom'])  . '</td>';
		}


		echo "<td>";

		if($nomfor['nom_formation'] != NULL)

		{

			echo $nomfor['nom_formation'] ;}

		else {

			echo ' -- Non renseigné';}

		echo "</td>";
		echo "<td>" . $date_de_demande_de_lagentr . "</td>";
		echo "<td>" . $key['date_non_precise_1'] . "</td>";
		echo "<td>" . $lieux . "</td>";

		echo "<td>";

		if($fournisseur['lib'] != NULL)

		{

			echo $fournisseur['lib'] ;}

		else {

			echo ' -- Non renseigné';}

		echo "</td>";




		echo '</tr>';
		$i++;



		if($key['mobilier_dif'] == TRUE) {
			$dif = 'Oui';
		}
		else {
			$dif = 'Non';
		}


		echo '
<tr class="details">


        <td colspan="5"> <h3>Détails : </h3>
			<div>

 ';

		if ($qualif_profil != 3) {
			echo  '<b>Agent :</b>' . ucfirst($emplye_rq['nom']) . ' ' . ucfirst($emplye_rq['prenom']);
			echo '<br />Image associée : <a href="uploads/'.$key['image'].'" ><img src="uploads/'.$key['image'].'" style="width:200px;height:200px;"> <br />( Document ) </img></a>';
		}

		echo ' <br /><b>Début demandé (1) :</b>  ' . $date_debut . '<br />
							<b>Fin demandé (1): </b>'. $date_fin .'

							<br />
';
// on met des petits liens modifier et supprimer pour l'administrateur

		if ($qualif_profil == 4) {
			echo '<a href="modifier_demande.php?id_demande='.$key['id'].'">Modifier</a> ';
			echo '<a href="suppr_dem_formation.php?id_demande='.$key['id'].'">Supprimer</a> ';
		}



		echo '
							<p>L\'agent mobilise-t-il son DIF ?<b> ' .$dif  . ' </b></p>
</div>
        </td>
           
							
       
    </tr>';


	}

	echo "</tbody></table>";

}







?>



<?php


switch ($profil) {
	// si on est sur un profil direction
	case 1:
		echo "<p>Voici les demandes de formation </p>";
		echo tableau($profil, $id_util);
		# code...
		break;
	// si on est sur un profil chef de service

	case 2:
		echo "<p>Voici les demandes de formation de vos employés </p>";
		echo tableau($profil, $id_util);

		echo "<p>Voici vos propres demandes :  </p>";
		echo tableau('1', $id_util);

		break;

	// si on est sur un profil agent
	case 3:
		echo "<p>Voici vos demandes de formation </p>";
		echo tableau($profil, $id_util);



		break;

	// si on est sur un profil administrateur (secrétaire)
	case 4:
		echo "<p>Formation disponibles :  </p>";
		echo tableau($profil, $id_util);
		break;


	default:
		echo "Erreur";
		break;
}

?>


	<br />




<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>