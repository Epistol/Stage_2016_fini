<?php


// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Modifier un utilisateur";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


// si on est pas sur un profil administrateur, on redirige la personne sur son compte
if ($profil != 4) {
	header('Location: compte.php');

}




$agent = connect_table_where('agent','id',$_GET['id']);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {



	if(isset($_POST['nom'])){
		if (!empty($_POST['nom'])) {
			$nom =  strtolower(htmlspecialchars($_POST['nom']));

		}
	}
	if(isset($_POST['service'])){
		if (!empty($_POST['service'])) {
			$service =  htmlspecialchars($_POST['service']);

		}
	}
	if(isset($_POST['qualification'])){
		if (!empty($_POST['qualification'])) {
			$qualification =  htmlspecialchars($_POST['qualification']);

		}
	}
	if(isset($_POST['profil'])){
		if (!empty($_POST['profil'])) {
			$profil =  htmlspecialchars($_POST['profil']);

		}
	}
	if(isset($_POST['grade'])){
		if (!empty($_POST['grade'])) {
			$grade =  htmlspecialchars($_POST['grade']);

		}
	}
	if(isset($_POST['dgfipid'])){
		if (!empty($_POST['dgfipid'])) {
			$dgfipid =  htmlspecialchars($_POST['dgfipid']);

		}
	}

	if(isset($_POST['ddn'])){

		$ddn =  htmlspecialchars($_POST['ddn']);


	}
	if(isset($_POST['prenom'])){
		if (!empty($_POST['prenom'])) {
			$prenom = strtolower(htmlspecialchars($_POST['prenom']));
		}
	}
	if(isset($_POST['password'])){
		if (!empty($_POST['password'])) {
			$mdp_pre = htmlspecialchars($_POST['password']);


			$mdp = crypt($mdp_pre, 'dgfip_63');


		}
	}

	$mail_agent = $prenom.'.'.$nom.'@dgfip.finances.gouv.fr';



	$stmt = $la_connexion->prepare("INSERT INTO agent (id,ddn,nom,prenom,mail,mdp,grade, qualif,profil, service) VALUES (:id,:ddn,:nom, :prenom,:mail, :mdp,:grade,:qualif,:profil, :service)");
	$stmt->bindParam(':id', $dgfipid);
	$stmt->bindParam(':ddn', $ddn);
	$stmt->bindParam(':mail', $mail_agent);
	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':mdp', $mdp);
	$stmt->bindParam(':grade', $grade);
	$stmt->bindParam(':qualif', $qualification);
	$stmt->bindParam(':profil', $profil);
	$stmt->bindParam(':service', $service);
	$stmt->execute();

}


// LE CONTENU :
?>




	<p>Modfier un agent : </p>

	<form method="post" class="pure-form pure-form-aligned" action="<?php $_SERVER['PHP_SELF']?>">
		<fieldset>
			<div class="pure-control-group"> <label for="nom">ID DGFiP: </label> <input name ="dgfipid" id="dgfipid" type="text" required placeholder="" value=
				"<?php echo $agent[0]['id']; ?>"> </div>
			<div class="pure-control-group"> <label for="nom">Nom : </label> <input name ="nom" id="nom" type="text" required placeholder=""value=
				"<?php echo $agent[0]['nom']; ?>"> </div>
			<div class="pure-control-group"> <label for="prenom">Prenom : </label> <input name ="prenom" id="prenom" type="text" required placeholder="" value=
				"<?php echo $agent[0]['prenom']; ?>"> </div>
			<div class="pure-control-group"> <label for="password">Mot de passe : </label> <input id="password" name="password" type="password" value=
				"<?php echo $agent[0]['mdp']; ?>" required placeholder="">
			</div>
			<div class="pure-control-group"> <label for="ddn">Date de naissance: </label> <input name ="ddn" id="ddn" type="text" required placeholder="" value=
				"<?php echo $agent[0]['ddn']; ?>"> </div>

			<div class="pure-control-group">
				<label for="service">Service : </label>
				<select name="service" id="service">
					<?php

					$service = connect_table('service');


					foreach ($service as $row){

						if($row['id'] == $agent[0]['service']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}


						echo '<option ' . $var_select . '  value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="qualification">Qualification : </label>
				<select name="qualification" id="qualification">
					<?php

					$qualif = connect_table('qualification');


					foreach ($qualif as $row){

						if($row['id'] == $agent[0]['qualif']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}


						echo '<option ' . $var_select . ' value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="grade">Grade : </label>
				<select name="grade" id="grade">
					<?php

					$grade = connect_table('grade');


					foreach ($grade as $row){

						if($row['id'] == $agent[0]['grade']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}


						echo '<option ' . $var_select . ' value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>
			<div class="pure-control-group">
				<label for="profil">Profil : </label>
				<select name="profil" id="profil">
					<?php

					$profil_t = connect_table('profil');


					foreach ($profil_t as $row){


						if($row['id'] == $agent[0]['profil']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}



						echo '<option ' . $var_select . '  value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>


			<button type="submit" class="pure-button pure-button-primary">Envoyer</button>
		</fieldset>
	</form>



<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>