<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Accueil";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';




// LE HEADER
require_once 'inc/header.php';


if (!isset($_SESSION['user'])) {
    header('Location: login.php');
}
else{
    header('Location: compte.php');
}




// LE CONTENU :


?>



<p>Bonjour et bienvenue, ici, vous avez accès aux formulaires de demandes de formation. <br /><br />
    Pour l'instant, toutes les formations ne sont pas enregistrées :( Mais vous avez accès au référentiel sur le site :  ! <br />
    Il vous suffira alors de reporter le code lié à la formation et d'y inscrire son intitulé si le choix ne vous est pas proposé.</p>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>

