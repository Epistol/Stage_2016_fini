<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Modifier demande de formation";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


// on met l'id de la session utilisateur dans une variable
$id_util = $_SESSION['user'];


// initialisation de la variable pour que le 'where' fonctionne
$var_assoc = 'id';

// si on est sur le profil admin
if ($profil != 4) {

	header('Location : compte.php');
}


$dem_formation = connect_table_where('demande_formation','id',$_GET['id_demande'] );

// requête select * des demandes de formation executé dans $dem_formation



// on récupère les données de la table formation en array
$formation = connect_table('formation');
//pareil pour les employés ou l'employé en cours sera l'agent qui est liée à la demande de formation
$employe = connect_table_where('agent','id',$dem_formation[0]['id_agent']);


$id_demande = $_GET['id_demande'];



if ($_SERVER['REQUEST_METHOD'] == 'POST') {


	// tout les name des input du formulaire stocké dans un array :




	$uploaddir = 'uploads/';
	$nomfichier = rand(42,9999999999) . basename($_FILES['document']['name']);
	$uploadfile = $uploaddir . $nomfichier ;


	move_uploaded_file($_FILES['document']['tmp_name'], $uploadfile);





	// CHAMP 1

	if (isset($_POST['agent'])) {
		$agent = htmlspecialchars($_POST['agent']);
	}
	else {
		$agent = NULL;
	}

	// CHAMP 2
	if (isset($_POST['code_action'])) {
		$code_action = htmlspecialchars($_POST['code_action']);
	}
	else {
		$code_action = NULL;
	}

	// CHAMP 3
	if (isset($_POST['fournisseur'])) {
		$fournisseur = htmlspecialchars($_POST['fournisseur']);
	}
	else {
		$fournisseur = NULL;
	}

	// CHAMP 3
	if (isset($_POST['affectation'])) {
		$affectation = htmlspecialchars($_POST['affectation']);
	}
	else {
		$affectation  = NULL;
	}
	// CHAMP 4
	if (isset($_POST['attentes'])) {
		$attentes = htmlspecialchars($_POST['attentes']);
	}
	else {
		$attentes = NULL;
	}

	// CHAMP 5
	if (isset($_POST['bilan_choix1'])) {
		$bilan_choix1 = htmlspecialchars($_POST['bilan_choix1']);
	}
	else {
		$bilan_choix1 = NULL;
	}


	// CHAMP 6
	if (isset($_POST['bilan_choix2'])) {
		$bilan_choix2 = htmlspecialchars($_POST['bilan_choix2']);
	}
	else {
		$bilan_choix2 = NULL;
	}

	// CHAMP 7
	if (isset($_POST['datedebut']) && $_POST['datedebut'] != '') {
		$datedebut = htmlspecialchars($_POST['datedebut']);
		$datedebut2 = explode("-", $datedebut);
		$datedebut = $datedebut2[2] ."-". $datedebut2[1] ."-".$datedebut2[0];
	}
	else {
		$datedebut = NULL;
	}

	// CHAMP 8
	if (isset($_POST['datefin']) && $_POST['datefin'] != '') {
		$datefin = htmlspecialchars($_POST['datefin']);
		$datefin2 = explode("-", $datefin);
		$datefin = $datefin2[2] ."-". $datefin2[1] ."-".$datefin2[0];
	}
	else {
		$datefin = NULL;
	}
	// CHAMP
	if (isset($_POST['date_inconnu'])) {
		$date_inconnu = htmlspecialchars($_POST['date_inconnu']);
	}
	else {
		$date_inconnu = NULL;
	}

	// CHAMP
	if (isset($_POST['ville'])) {
		$ville = htmlspecialchars($_POST['ville']);
	}
	else if(isset($_POST['zone_inconnu'])) {
		$ville = htmlspecialchars($_POST['zone_inconnu']);
	}
	else {
		$ville = NULL;
	}

	// CHAMP

	if (isset($_POST['datedebut2']) && $_POST['datedebut2'] != '') {
		$datedebut2 = htmlspecialchars($_POST['datefin']);
		$datedebut3 = explode("-", $datedebut2);
		$datedebut2 = $datedebut3[2] ."-". $datedebut3[1] ."-".$datedebut3[0];
	}

	else {
		$datedebut2 = NULL;
	}

	// CHAMP


	if (isset($_POST['datefin2']) && $_POST['datefin2'] != '') {
		$datefin2 = htmlspecialchars($_POST['datefin2']);
		$datefin3 = explode("-", $datefin2);
		$datefin2 = $datefin3[2] ."-". $datefin3[1] ."-".$datefin3[0];
	}


	else {
		$datefin2 = NULL;
	}

	// CHAMP
	if (isset($_POST['date_inconnu2'])) {
		$date_inconnu2 = htmlspecialchars($_POST['date_inconnu2']);
	}
	else {
		$date_inconnu2 = NULL;
	}


	// CHAMP
	if (isset($_POST['ville2'])) {
		$ville2 = htmlspecialchars($_POST['ville2']);
	}
	else if(isset($_POST['zone_inconnu2'])) {
		$ville2 = htmlspecialchars($_POST['zone_inconnu2']);
	}
	else {
		$ville2 = NULL;
	}



	// CHAMP
	if (isset($_POST['dif_choix'])) {
		$dif_choix = htmlspecialchars($_POST['dif_choix']);
	}
	else {
		$dif_choix = 0;
	}

	// CHAMP
	if (isset($_POST['tps_travail'])) {
		$tps_travail = htmlspecialchars($_POST['tps_travail']);
	}
	else {
		$tps_travail = NULL;
	}
	// CHAMP



	if (isset($_POST['datedemande']) && $_POST['datedemande'] != '') {
		$datedemande = htmlspecialchars($_POST['datedemande']);
		$datedemande2 = explode("-", $datedemande);
		$datedemande = $datedemande2[2] ."-". $datedemande[1] ."-".$datedemande[0];
	}

	else {
		$datedemande = NULL;
	}


	// CHAMP
	if (isset($_POST['qualif_t'])) {
		$qualif_t = htmlspecialchars($_POST['qualif_t']);
	}
	else {
		$qualif_t = NULL;
	}

	// CHAMP
	if (isset($_POST['decision_choix'])) {
		$decision_choix = htmlspecialchars($_POST['decision_choix']);
	}
	else {
		$decision_choix = NULL;
	}

	// CHAMP
	if (isset($_POST['motif_refus'])) {
		$motif_refus = htmlspecialchars($_POST['motif_refus']);
	}
	else {
		$motif_refus = NULL;
	}

	// CHAMP
	if (isset($_POST['decision_choix_dir'])) {
		$decision_choix_dir = htmlspecialchars($_POST['decision_choix_dir']);
	}
	else {
		$decision_choix_dir = NULL;
	}

	// CHAMP
	if (isset($_POST['motif_refus_dir'])) {
		$motif_refus_dir = htmlspecialchars($_POST['motif_refus_dir']);
	}
	else {
		$motif_refus_dir = NULL;
	}
	// CHAMP
	if (isset($_POST['priorite_1'])) {
		$priorite_1 = htmlspecialchars($_POST['priorite_1']);
	}
	else {
		$priorite_1 = NULL;
	}

	// CHAMP
	if (isset($_POST['priorite_2'])) {
		$priorite_2 = htmlspecialchars($_POST['priorite_2']);
	}
	else {
		$priorite_2 = NULL;
	}
// CHAMP 
	if (isset($_POST['etat2'])) {
		$etat2 = htmlspecialchars($_POST['etat2']);
	}
	else {
		$etat2 = 0;
	}


	$date_ajout_demande = date("Y-m-d");





	$requete = "UPDATE demande_formation SET id_agent = :id_agent, id_formation = :id_formation, id_fournisseur = :id_fournisseur ,objectif_stagiaire = :objectif_stagiaire, reforme_structure_bilan_theorique = :reforme_structure_bilan_theorique , reforme_strucure_pif = :reforme_strucure_pif, date_choix_debut_1 = :date_choix_debut_1, date_choix_fin_1 = :date_choix_fin_1, date_non_precise_1 = :date_non_precise_1, date_choix_debut_2 = :date_choix_debut_2, date_choix_fin_2 = :date_choix_fin_2, 

			date_inconnue_2 = :date_inconnue_2, lieux_1 = :lieux_1, lieux_2 = :lieux_2, mobilier_dif = :mobilier_dif, formation_hors_temps = :formation_hors_temps, dtae_demande_agent = :dtae_demande_agent, qualification_chef = :qualification_chef, decision_chef_service = :decision_chef_service, decision_direction = :decision_direction, motif_refus_chef_service = :motif_refus_chef_service, motif_refus_direction = :motif_refus_direction,  date_ajout_demande = :date_ajout_demande,affectation = :affectation,priorite_chef_service = :priorite_chef_service,priorite_direction = :priorite_direction,image = :image,status_formation = :status_formation WHERE id = :id_demande_requete";

	$stmt = $la_connexion->prepare($requete);



	$stmt->bindParam(':id_demande_requete', $_GET['id_demande']);
	$stmt->bindParam(':id_agent', $agent);
	$stmt->bindParam(':id_formation', $code_action);
	$stmt->bindParam(':id_fournisseur', $fournisseur);
	$stmt->bindParam(':objectif_stagiaire', $attentes);
	$stmt->bindParam(':reforme_structure_bilan_theorique', $bilan_choix1);
	$stmt->bindParam(':reforme_strucure_pif', $bilan_choix2);
	$stmt->bindParam(':date_choix_debut_1', $datedebut);
	$stmt->bindParam(':date_choix_fin_1', $datefin);
	$stmt->bindParam(':date_non_precise_1', $date_inconnu);
	$stmt->bindParam(':date_choix_debut_2', $datedebut2);
	$stmt->bindParam(':date_choix_fin_2', $datefin2);
	$stmt->bindParam(':date_inconnue_2', $date_inconnu2);
	$stmt->bindParam(':lieux_1', $ville);
	$stmt->bindParam(':lieux_2', $ville2);
	$stmt->bindParam(':mobilier_dif', $dif_choix);
	$stmt->bindParam(':formation_hors_temps', $tps_travail);
	$stmt->bindParam(':dtae_demande_agent', $datedemande);
	$stmt->bindParam(':qualification_chef', $qualif_t);
	$stmt->bindParam(':decision_chef_service', $decision_choix);
	$stmt->bindParam(':decision_direction', $decision_choix_dir);
	$stmt->bindParam(':motif_refus_chef_service', $motif_refus);
	$stmt->bindParam(':motif_refus_direction', $motif_refus_dir);
	$stmt->bindParam(':date_ajout_demande', $date_ajout_demande);
	$stmt->bindParam(':affectation', $affectation);
	$stmt->bindParam(':priorite_chef_service', $priorite_1);
	$stmt->bindParam(':priorite_direction', $priorite_2);
	$stmt->bindParam(':image', $nomfichier);
	$stmt->bindParam(':status_formation', $etat2);

	$stmt->execute();



	echo "Votre modification à bien été prise en compte.";


	require_once 'inc/footer.php';
	exit();
}

// Vérification des variables


if ($profil !=3) {
	// histoire d'afficher les infos de l'employé et le scan de la formation
	echo  'Nom : ' . ucfirst($employe[0]['nom']) . ' Prenom : ' . ucfirst($employe[0]['prenom']);
	echo '<br />Image associée : <a href="uploads/'.$dem_formation[0]['image'].'" ><img src="uploads/'.$dem_formation[0]['image'].'" style="width:200px;height:200px;"/>Document</a>';
}




// le formulaire 'normal' commence ici
?>



<form enctype="multipart/form-data" class="pure-form pure-form-aligned" method="post">


	<legend for="document">Vous avez un scan à envoyer plutôt que des infos à rentrer ? </legend>
	<input class="input-group" type="file" name="document" id="document" />


	<fieldset>
		<legend><u>Agent</u></legend>


		<fieldset>
			<legend>
				Action demandée
			</legend>
			<div class="pure-control-group"> <label for="code_action">Code de l'action</label>

				<select name="code_action" id="code_action">

					<option value="999">Autre</option>


					<?php

					foreach ($formation as $row){

						// on met un 'selected' pour afficher directement depuis la bdd l'id de formation associé à la demande
						if($row['id'] == $dem_formation[0]['id_formation']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';
						}

						echo '<option ' . $var_select . ' value="' . $row['id'] . '">' . ucfirst($row['type_formation']). ' - ' . ucfirst($row['nom_formation']) . '</option>';

					}

					?>

				</select> </div>


			<div class="pure-control-group">
				<label for="fournisseur">Fournisseur :  </label>
				<select name="fournisseur" id="fournisseur">
					<option value="999">Autre /  Inconnu</option>
					<?php


					$fournisseur = connect_table('fournisseur');
					// on met un 'selected' pour afficher directement depuis la bdd l'id du fournisseur associé à la demande
					foreach ($fournisseur as $row){
						if($row['id'] == $dem_formation[0]['id_fournisseur']){
							$var_select = 'selected';
						}
						else {
							$var_select = '';

						}


						echo '<option ' . $var_select . ' value="' . $row['id'] . '">' . ucfirst($row['lib']). '</option>';

					} ?>

				</select>
			</div>

			<div class="pure-control-group"> <label for="affectation">Affectation (formulaire 2014)</label>


				<?php
				// permet de 'checked' automatiquement les cases déjà checké depuis la bdd
				if($dem_formation[0]['affectation'] == 1 ){
					$check1 = 'checked';
				}
				else if($dem_formation[0]['affectation'] == 2 ){
					$check2 = 'checked';
				}
				else {
					$check1 = '';
					$check2 = '';
				}


				?>


				<label for="affect_1" class="pure-checkbox"> <input id="affect_1" name="affectation" type="checkbox" value="1" <?php echo $check1; ?> >Administration centrale </label>
				<label for="affect_2" class="pure-checkbox"> <input id="affect_2" name="affectation" type="checkbox" value="2" <?php echo $check2; ?> > Service déconcentrés </label>


			</div>





		</fieldset>




		<fieldset>
			<legend>
				Objectif du stagiaire
			</legend>

		 <textarea class="pure-input-1" placeholder="" name="attentes" ><?php
			 // on affiche depuis la bdd le champ complété ou non
			 echo $dem_formation[0]['objectif_stagiaire'] ?></textarea> <br /><br />

			<label >Si vous êtes concerné par une réforme de structures, avez-vous bénéficié : </label>


			<?php
			// permet de 'checked' automatiquement les cases déjà checké depuis la bdd avec des variables uniques
			if($dem_formation[0]['reforme_structure_bilan_theorique'] == 1 ){
				$check3 = 'checked';
			}
			else if($dem_formation[0]['reforme_structure_pif'] == 2 ){
				$check4 = 'checked';
			}
			else {
				$check3 = '';
				$check4 = '';
			}


			?>
			<label for="bilan_choix_1" class="pure-checkbox"> <input id="bilan_choix_1"  <?php echo $check3; ?> name="bilan_choix1" type="checkbox" value="1"> d'un bilan préalable de vos connaissances théoriques ou pratiques </label>
			<label for="bilan_choix_2" class="pure-checkbox"> <input id="bilan_choix_2"  <?php echo $check4; ?> name="bilan_choix2" type="checkbox" value="1"> d'un plan individuel de formation </label>

		</fieldset>


		<fieldset class="pure-group">



			<!-- Partie date -->
			<legend>Préférences sur la date et la localisation de l'action</legend>

			<fieldset>
				<legend>Choix 1 : </legend>
				<fieldset class="pure-group">
					<div class="pure-control-group">
						<input name="datedebut" id="datedebut" class="dateTxt" type="text" placeholder="Date de début"></div>
					<div class="pure-control-group">   <input value="<?php echo $dem_formation[0]['date_choix_debut_1'] ?>" name="datefin" placeholder="Date de fin" id="datefin" class="dateTxt" type="text" >
					</div>
					<div class="pure-control-group"> <input value="<?php echo $dem_formation[0]['date_non_precise_1'] ?>"  id="datedefin_inconnu" name="datedefin_inconnu" type="text" > </div>
				</fieldset>
				<div class="pure-control-group">
					<label  for="ville">Ville : </label>
					<input name="ville" id="ville" type="text" placeholder="" value="<?php echo $dem_formation[0]['lieux_1'] ?>" >
				</div>



				<div class="pure-control-group">
					<label for="zone_inconnu">Si inconnue : </label>
					<input name="zone_inconnu" id="zone_inconnu" type="text" placeholder="saisissez le lieu">
				</div>

			</fieldset>

			<fieldset>
				<legend>Choix 2 : </legend>
				<div class="pure-control-group">
					<label for="datedebut2">Date de début : </label>  <input name="datedebut2" id="datedebut2" class="dateTxt" type="text" ></div>
				<div class="pure-control-group">  <label for="datefin2">Date de fin : </label>  <input name="datefin2" id="datefin2" class="dateTxt" type="text" >
				</div>
				<div class="pure-control-group"> <label for="date_inconnu2">Si inconnue : </label> <input value="<?php echo $dem_formation[0]['date_inconnue_2'] ?>"  id="date_inconnu2" name="date_inconnu2" type="text" placeholder=""> </div>
				<div class="pure-control-group">
					<label for="ville2">Ville : </label>
					<input id="ville2" type="text" placeholder="" value="<?php echo $dem_formation[0]['lieux_2'] ?>" >
				</div>

				<div class="pure-control-group"> <label for="zone_inconnu2">Si inconnue : </label> <input name="zone_inconnu2" id="zone_inconnu2" type="text" placeholder="saisissez le lieu"> </div>

			</fieldset>



		</fieldset>
		<fieldset>
			<legend>Demande de mobilisation du droit individuel à la formation (DIF)</legend>
			<div class="pure-control-group"> <label for="dif_choix">L'agent souhaite-t-il mobiliser son DIF ? </label>
				<label for="dif_1" class="pure-checkbox"> <input id="dif_1" name="dif_choix" type="radio" value="1">Oui </label>
				<label for="dif_2" class="pure-checkbox"> <input id="dif_2" name="dif_choix" type="radio" value="0">Non </label>


			</div>


			<div class="pure-control-group"> <label for="tps_travail">La formation au titre du DIF sera-t-elle suivie hors du travail ? </label>
				<label for="tps_travail1" class="pure-checkbox"> <input id="tps_travail1" name="tps_travail" type="radio" value="1">Oui </label>
				<label for="tps_travail2" class="pure-checkbox"> <input id="tps_travail2" name="tps_travail" type="radio" value="0">Non </label>


			</div>


		</fieldset>
		<fieldset>
			<legend>Date de demande : </legend>
			<div class="pure-control-group">
				<label for="datedemande">Date de demande : </label>  <input name="datedemande" id="datedemande" class="dateTxt" type="text" ></div>
		</fieldset>



	</fieldset>




	<fieldset>
		<legend><u>Décision</u></legend>
		<fieldset>
			<div class="pure-control-group">

				<legend  for="qualif_t">Qualification de l'objectif de la formation par le chef de service : </legend>
				<label for="qualif_t1" class="pure-checkbox"> <input id="qualif_t1" name="qualif_t" type="checkbox" value="1"> T1 </label>
				<label for="qualif_t2" class="pure-checkbox"> <input id="qualif_t2" name="qualif_t" type="checkbox" value="2"> T2 </label>
				<label for="qualif_t3" class="pure-checkbox"> <input id="qualif_t3" name="qualif_t" type="checkbox" value="3"> T3 </label>


		</fieldset>
		<fieldset>
			<legend>Décision sur la demande de formation</legend>
			<div class="pure-control-group">

				<label for="decision_choix" >Avis du chef de service </label>
				<label for="decision_choix1" class="pure-checkbox"> <input id="decision_choix1" name="decision_choix" type="checkbox" value="1"> Acceptée </label>

				<select name="priorite_1" id="priorite_1">
					<?php

					$motifrefus  = array('Priorité','P1 : urgente pour la gestion du poste','P2 : peut être différée dans l\'année','P3 : peut être differée l\'année suivante' );
					$i =0;
					foreach ($motifrefus as $row1){
						$i++;
						echo '<option value="' . $i . '">' . $row1. '</option>';

					} ?>
				</select>



				<label for="decision_choix2" class="pure-checkbox"> <input id="decision_choix2" name="decision_choix" type="checkbox" value="0"> Refusée </label>







				<select name="motif_refus" id="motif_refus">
					<?php

					$motifrefus  = array('Pas de refus', 'Stage non justifié par rapport à l\'activité de l\'agent', 'Stage non opportun au regard des nécéssités de service',
						'Candidature non prioritaire', 'Agent ne disposant pas du pré-requis', 'Nombre de candidats insuffisant pour ouvrir la session', 'Autre motif' );
					$i =0;
					foreach ($motifrefus as $row1){

						echo '<option value="' . $i . '">' . $row1. '</option>';
						$i++;
					} ?>
				</select>

			</div>
			<div class="pure-control-group">
				<label for="decision_choix_dir" >Décision de la direction </label>
				<label for="decision_choix1_dir" class="pure-checkbox"> <input id="decision_choix1_dir" name="decision_choix_dir"
																			   type="checkbox" value="1"> Acceptée </label>

				<select name="priorite_2" id="priorite_2">
					<?php

					$motifrefus  = array('Priorité','P1 : urgente pour la gestion du poste','P2 : peut être différée dans l\'année','P3 : peut être differée l\'année suivante' );
					$i =0;
					foreach ($motifrefus as $row1){

						echo '<option value="' . $i . '">' . $row1. '</option>';
						$i++;
					} ?>
				</select>



				<label for="decision_choix2_dir" class="pure-checkbox"> <input id="decision_choix2_dir" name="decision_choix_dir" type="checkbox" value="2"> Refusée </label>




				<select name="motif_refus_dir" id="motif_refus_dir">
					<?php
					$j = 0;
					$motifrefus  = array('Autre motif','Pas de refus', 'Stage non justifié par rapport à l\'activité de l\'agent', 'Stage non opportun au regard des nécéssités de service',
						'Candidature non prioritaire', 'Agent ne disposant pas du pré-requis', 'Nombre de candidats insuffisant pour ouvrir la session'  );
					foreach ($motifrefus as $row1){
						$j++;
						echo '<option value="' . $j . '">' . $row1. '</option>';

					} ?>
				</select>

			</div>
		</fieldset>

		<input type="hidden" name="agent" value="<?php echo $employe[0]['id']; ?>">

	</fieldset>




	<fieldset>
		<legend>Etat de la formation</legend>
		<div class="pure-control-group">

			<label for="etat" >La formation à t-elle été réalisée ? </label>

			<select name="etat2" id="etat2">
				<?php

				$etat  = array('Etat','Réalisé','Annulée','Reportée' );
				$i =0;
				foreach ($etat as $row1){

					echo '<option value="' . $i . '">' . $row1. '</option>';
					$i++;
				} ?>
			</select>


		</div>


	</fieldset>


	<button type="submit1" class="pure-button pure-button-primary">Envoyer</button>



</form>





<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>

