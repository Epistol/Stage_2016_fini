<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Accueil";

//NOS PETITES FONCTIONS
require_once '../inc/config.php';





// LE CONTENU :

$requete = connect_table_sans_fetch('formation');

?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Calendrier</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(function($){
            $('.month').hide();
            $('.month:first').show();
            $('.months a:first').addClass('active');
            var current = 1;
            $('.months a').click(function(){
                var month = $(this).attr('id').replace('linkMonth','');
                if(month != current){
                    $('#month'+current).slideUp();
                    $('#month'+month).slideDown();
                    $('.months a').removeClass('active');
                    $('.months a#linkMonth'+month).addClass('active');
                    current = month;
                }
                return false;
            });
        });
    </script>
</head>
<body>



<?php
// on requiert date.php (fonction de date et classe)
require_once('date.php');
// nouvel objet Date
$date = new Date();
// année : année en cours
$year = date('Y');

if(isset($_GET['annee'])){
    $year = $_GET['annee'];
}


// on récupère les évènements stockées
$events = $date->getEvents($requete,$year);

// afficher les dates
$dates = $date->getAll($year);
?>

<!--  AFFICHE ICI LE CALENDRIER -->
<div class="periods">
    <!-- affiche le nom de l'année choisie -->
    <div class="year"><?php echo $year; ?></div>
    <!-- affiche les mois-->
    <div class="months">
        <ul>
            <?php foreach ($date->months as $id=>$m): ?>
                <li><a href="#" id="linkMonth<?php echo $id+1; ?>"><?php echo utf8_encode(substr(utf8_decode($m),0,4)); ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="clear"></div>
    <!-- stocke les dates de l'année en cours -->
    <?php $dates = current($dates); ?>
    <!-- pour chaque date on l'affiche en tableau -->
    <?php foreach ($dates as $m=>$days): ?>
        <div class="month relative" id="month<?php echo $m; ?>">
            <table>
                <thead>
                <tr>

                    <?php
                    // affiche le jour de la semaine
                    foreach ($date->days as $d): ?>
                        <th><?php echo substr($d,0,3); ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php $end = end($days); foreach($days as $d=>$w): ?>
                    <?php $time = strtotime("$year-$m-$d"); ?>
                    <?php if($d == 1 && $w != 1): ?>
                        <td colspan="<?php echo $w-1; ?>" class="padding"></td>
                    <?php endif; ?>
                    <!-- Si le jour est celui d'ajd, on change la class du td.-->
                    <td <?php if($time == strtotime(date('Y-m-d'))): ?> class="today" <?php endif; ?> >
                        <div class="relative">
                            <div class="day"><?php echo $d; ?></div>
                        </div>
                        <div class="daytitle">
                            <?php echo $date->days[$w-1]; ?> <?php echo $d; ?>  <?php echo $date->months[$m-1]; ?>
                        </div>
                        <ul class="events">
                            <?php if(isset($events[$time])): foreach($events[$time] as $e): ?>
                                <li>Formation : <?php echo $e; ?></li>

                            <?php endforeach; endif;  ?>
                        </ul>
                    </td>

                    <?php
                    // si on arrive à la fin d'une semaine, on ferme le tr
                    if($w == 7): ?>
                </tr><tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if($end != 7): ?>
                        <td colspan="<?php echo 7-$end; ?>" class="padding"></td>
                    <?php endif; ?>
                </tr>
                </tbody>
            </table>
        </div>
    <?php endforeach; ?>
</div>
<div class="clear"></div>



</body>
</html>