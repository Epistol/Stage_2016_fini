<?php


// Création de la class Date, qui est un objet qui possède 
class Date{

// 2 variables : 
    // une variable array de jours
    var $days       = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche');
    // une variable array de mois
    var $months     = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');





    function getEvents($requete, $year){
        $r = array();

        /** * Ce que je veux
        $r['TIMESTAMP']['id'] = title ;
         */

        while($d = $requete->fetch() ) {
            $r[strtotime($d['date_debut'])][$d['id']] = $d['nom_formation'];

        }
        return $r;

    }


    function getAll($year){
        $r = array();
        /**
         * Boucle version procédurale
         *
        $date = strtotime($year.'-01-01');
        while(date('Y',$date) <= $year){
        // Ce que je veux => $r[ANEEE][MOIS][JOUR] = JOUR DE LA SEMAINE
        $y = date('Y',$date);
        $m = date('n',$date);
        $d = date('j',$date);
        $w = str_replace('0','7',date('w',$date));
        $r[$y][$m][$d] = $w;
        $date = strtotime(date('Y-m-d',$date).' +1 DAY');
        }
         *
         *
         */
        $date = new DateTime($year.'-01-01');
        while($date->format('Y') <= $year){
            // Ce que je veux => $r[ANEEE][MOIS][JOUR] = JOUR DE LA SEMAINE
            $y = $date->format('Y');
            $m = $date->format('n');
            $d = $date->format('j');
            $w = str_replace('0','7',$date->format('w'));
            $r[$y][$m][$d] = $w;
            $date->add(new DateInterval('P1D'));
        }
        return $r;
    }

}
?>
