<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Traitement";

//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';





if ($_SERVER['REQUEST_METHOD'] == 'POST') {


	// tout les name des input du formulaire stocké dans un array : 




	$uploaddir = 'uploads/';
	$nomfichier = rand(42,9999999999) . basename($_FILES['document']['name']);
	$uploadfile = $uploaddir . $nomfichier ;


	move_uploaded_file($_FILES['document']['tmp_name'], $uploadfile);





// CHAMP 1

	if (isset($_POST['agent'])) {
		$agent = htmlspecialchars($_POST['agent']);
	}
	else {
		$agent = NULL;
	}

// CHAMP 2
	if (isset($_POST['code_action'])) {
		$code_action = htmlspecialchars($_POST['code_action']);
	}
	else {
		$code_action = NULL;
	}

// CHAMP 3
	if (isset($_POST['fournisseur'])) {
		$fournisseur = htmlspecialchars($_POST['fournisseur']);
	}
	else {
		$fournisseur = NULL;
	}

// CHAMP 3
	if (isset($_POST['affectation'])) {
		$affectation = htmlspecialchars($_POST['affectation']);
	}
	else {
		$affectation  = NULL;
	}
// CHAMP 4
	if (isset($_POST['attentes'])) {
		$attentes = htmlspecialchars($_POST['attentes']);
	}
	else {
		$attentes = NULL;
	}

// CHAMP 5
	if (isset($_POST['bilan_choix1'])) {
		$bilan_choix1 = htmlspecialchars($_POST['bilan_choix1']);
	}
	else {
		$bilan_choix1 = NULL;
	}


// CHAMP 6
	if (isset($_POST['bilan_choix2'])) {
		$bilan_choix2 = htmlspecialchars($_POST['bilan_choix2']);
	}
	else {
		$bilan_choix2 = NULL;
	}

// CHAMP 7
	if (isset($_POST['datedebut']) && $_POST['datedebut'] != '') {
		$datedebut = htmlspecialchars($_POST['datedebut']);
		$datedebut2 = explode("-", $datedebut);
		$datedebut = $datedebut2[2] ."-". $datedebut2[1] ."-".$datedebut2[0];
	}
	else {
		$datedebut = NULL;
	}

// CHAMP 8
	if (isset($_POST['datefin']) && $_POST['datefin'] != '') {
		$datefin = htmlspecialchars($_POST['datefin']);
		$datefin2 = explode("-", $datefin);
		$datefin = $datefin2[2] ."-". $datefin2[1] ."-".$datefin2[0];
	}
	else {
		$datefin = NULL;
	}
// CHAMP 
	if (isset($_POST['date_inconnu'])) {
		$date_inconnu = htmlspecialchars($_POST['date_inconnu']);
	}
	else {
		$date_inconnu = NULL;
	}

// CHAMP
	if (isset($_POST['ville'])) {
		$ville = htmlspecialchars($_POST['ville']);
	}
	else if(isset($_POST['zone_inconnu'])) {
		$ville = htmlspecialchars($_POST['zone_inconnu']);
	}
	else {
		$ville = NULL;
	}

// CHAMP 

	if (isset($_POST['datedebut2']) && $_POST['datedebut2'] != '') {
		$datedebut2 = htmlspecialchars($_POST['datefin']);
		$datedebut3 = explode("-", $datedebut2);
		$datedebut2 = $datedebut3[2] ."-". $datedebut3[1] ."-".$datedebut3[0];
	}

	else {
		$datedebut2 = NULL;
	}

// CHAMP 


	if (isset($_POST['datefin2']) && $_POST['datefin2'] != '') {
		$datefin2 = htmlspecialchars($_POST['datefin2']);
		$datefin3 = explode("-", $datefin2);
		$datefin2 = $datefin3[2] ."-". $datefin3[1] ."-".$datefin3[0];
	}


	else {
		$datefin2 = NULL;
	}

// CHAMP 
	if (isset($_POST['date_inconnu2'])) {
		$date_inconnu2 = htmlspecialchars($_POST['date_inconnu2']);
	}
	else {
		$date_inconnu2 = NULL;
	}


// CHAMP
	if (isset($_POST['ville2'])) {
		$ville2 = htmlspecialchars($_POST['ville2']);
	}
	else if(isset($_POST['zone_inconnu2'])) {
		$ville2 = htmlspecialchars($_POST['zone_inconnu2']);
	}
	else {
		$ville2 = NULL;
	}



// CHAMP 
	if (isset($_POST['dif_choix'])) {
		$dif_choix = htmlspecialchars($_POST['dif_choix']);
	}
	else {
		$dif_choix = 0;
	}

// CHAMP 
	if (isset($_POST['tps_travail'])) {
		$tps_travail = htmlspecialchars($_POST['tps_travail']);
	}
	else {
		$tps_travail = NULL;
	}
// CHAMP 



	if (isset($_POST['datedemande']) && $_POST['datedemande'] != '') {
		$datedemande = htmlspecialchars($_POST['datedemande']);
		$datedemande2 = explode("-", $datedemande);
		$datedemande = $datedemande2[2] ."-". $datedemande[1] ."-".$datedemande[0];
	}

	else {
		$datedemande = NULL;
	}


// CHAMP 
	if (isset($_POST['qualif_t'])) {
		$qualif_t = htmlspecialchars($_POST['qualif_t']);
	}
	else {
		$qualif_t = NULL;
	}

// CHAMP 
	if (isset($_POST['decision_choix'])) {
		$decision_choix = htmlspecialchars($_POST['decision_choix']);
	}
	else {
		$decision_choix = NULL;
	}

// CHAMP 
	if (isset($_POST['motif_refus'])) {
		$motif_refus = htmlspecialchars($_POST['motif_refus']);
	}
	else {
		$motif_refus = NULL;
	}

// CHAMP 
	if (isset($_POST['decision_choix_dir'])) {
		$decision_choix_dir = htmlspecialchars($_POST['decision_choix_dir']);
	}
	else {
		$decision_choix_dir = NULL;
	}

// CHAMP 
	if (isset($_POST['motif_refus_dir'])) {
		$motif_refus_dir = htmlspecialchars($_POST['motif_refus_dir']);
	}
	else {
		$motif_refus_dir = NULL;
	}
// CHAMP 
	if (isset($_POST['priorite_1'])) {
		$priorite_1 = htmlspecialchars($_POST['priorite_1']);
	}
	else {
		$priorite_1 = NULL;
	}

// CHAMP 
	if (isset($_POST['priorite_2'])) {
		$priorite_2 = htmlspecialchars($_POST['priorite_2']);
	}
	else {
		$priorite_2 = NULL;
	}
// CHAMP 
	if (isset($_POST['etat2'])) {
		$etat2 = htmlspecialchars($_POST['etat2']);
	}
	else {
		$etat2 = 0;
	}


	$date_ajout_demande = date("Y-m-d");





	$requete = "INSERT INTO demande_formation (id_agent, id_formation, id_fournisseur,objectif_stagiaire, reforme_structure_bilan_theorique, reforme_strucure_pif, date_choix_debut_1, date_choix_fin_1, date_non_precise_1, date_choix_debut_2, date_choix_fin_2, date_inconnue_2, lieux_1, lieux_2, mobilier_dif, formation_hors_temps, dtae_demande_agent, qualification_chef, decision_chef_service, decision_direction, motif_refus_chef_service, motif_refus_direction,  date_ajout_demande,affectation,priorite_chef_service,priorite_direction,image,status_formation ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	$stmt = $la_connexion->prepare($requete);

	$stmt->execute(array($agent,$code_action,$fournisseur,$attentes,$bilan_choix1,$bilan_choix2,$datedebut,

		$datefin, $date_inconnu,

		$datedebut2, $datefin2,

		$date_inconnu2, $ville, $ville2,

		$dif_choix, $tps_travail,$datedemande,

		$qualif_t,$decision_choix,$decision_choix_dir,

		$motif_refus, $motif_refus_dir,

		$date_ajout_demande,$affectation,$priorite_1,$priorite_2,$nomfichier,$etat2));


	header('Location: compte.php');




}


?>


