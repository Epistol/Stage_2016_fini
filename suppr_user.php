<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Suppresion de demande de formation ...";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';


if (isset($_GET['id'] )) {

	$nom_formation = connect_table_where('agent','id',$_GET['id']);


	$id_form = $_GET['id'];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isset($_POST['submit'])) {

			$stmt = $la_connexion->prepare("DELETE FROM agent WHERE id = :id");

			$stmt->bindParam(':id', $id_form);
			$stmt->execute();
			header('Location: liste_agents.php');


		}
		else if(isset($_POST['non'])){
			header('Location: liste_agents.php');
		}
	}


}



?>

	<form class="pure-form pure-form-aligned" method="post">

		<div class="pure-controls">
			<p>Êtes-vous sur de vouloir supprimer l'utilisateur " <b><?php echo $nom_formation[0]['nom'].' ' .$nom_formation[0]['prenom']; ?> </b>"?</p>

			<button type="submit" name="submit" class="pure-button pure-button-primary">Oui</button>
			<button type="submit" name="non" class="pure-button pure-button-primary">Non</button>
		</div>
	</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>