<!doctype html>
<html class="no-js" lang="fr">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php
        // $varpage est une variable qui stocke le nom de la page, à placer dans chaque page .php, sinon par défaut on affiche DGFiP
        if(isset($varpage)){echo ucfirst($varpage). ' - DGFiP'; } else {echo 'DGFiP';}

        ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bibliothèques CSS -->
    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="./css/pure.css">

    <!-- Jquery et html5shiv (pour assurer la compatibilité ie8) -->
    <script src = "./js/html5shiv.js" > </script>
    <script src = "./js/jquery_ui/external/jquery/jquery.js" > </script>
    <script src = "./js/jquery-ui.js" > </script>

    <!-- Le date picker des champs à date, traduit en fr -->
    <script type="text/javascript">

        $(function(){
            $('.dateTxt').datepicker(); }
        );

        $.datepicker.setDefaults(
            {
                altField: "#datepicker",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy'
            }
        );

    </script>


    <!-- Script des tableaux pour afficher des 'details' sur chaque clique de ligne -->
    <script>
        $(document).ready(function(){
            $("#report tr:odd").addClass("master");
            $("#report tr:not(.master)").hide();
            $("#report tr:first-child").show("slow" );
            $("#report tr.master").click(function(){
                $(this).toggleClass("master-clicked");
                $(this).next("tr").fadeToggle( "slow", "linear" );
                $(this).find(".arrow").toggleClass("up");
            });
        });
    </script>


</head>
<body>

<?php

if (isset($_SESSION['user'])) {


// on stocke le profil de l'utilisateur dans la variable $profil
    $profil = profilagent($_SESSION['user']);
}
else {
    $profil = '';
}
?>


<div class="pure-g">

    <header class="pure-u-1 head_page">

        <!-- Title -->
        <div class="mdl-layout-title">
            <a href="./index.php" class="titre_header">
                Intranet du suivi des formations
            </a>
        </div>



        <nav class="menu-haut pure-menu pure-menu-horizontal">

            <ul class="pure-menu-list">

                <?php
                if (!isset($_SESSION['user'])) {
                    echo '
              <li class="pure-menu-item pure-menu-selected">
                <a href="./login.php" class="pure-menu-link navig_lien transition">Connexion</a>
              </li>
              ';
                }
                else {
                    echo '

              <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                <a href="compte.php" id="menuLink1" class="pure-menu-link">Votre compte</a>

                <ul class="pure-menu-children">
                  
    <li class="pure-menu-item"><a href="modif_mdp.php" class="pure-menu-link">Modifier votre mot de passe</a></li>

';

                    if($profil == 4){
                        echo '  <li class="pure-menu-item"><a href="register.php" class="pure-menu-link">Création nouvel utilisateur</a></li>';
                    }
                    echo '</ul>
                </li>

              ';
                    echo '
              <li class="pure-menu-item pure-menu-selected">
                <a href="./logout.php" class="pure-menu-link navig_lien transition">Déconnexion</a>
              </li>';
                    if($profil == 4){
                        echo '<li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                <a href="compte.php" id="menuLink1" class="pure-menu-link">Agents</a>

                <ul class="pure-menu-children">
                  
    <li class="pure-menu-item"><a href="liste_agents.php" class="pure-menu-link">Liste des agents</a></li>
  
</ul>
</li>
              ';

                        // <li class="pure-menu-item"><a href="crypt_mdp.php" class="pure-menu-link">Crypter mdp</a></li>
                    }
                }

                ?>

                <!-- Dropdown 1 -->
                <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                    <a href="#" id="menuLink1" class="pure-menu-link">Formations</a>

                    <ul class="pure-menu-children">
                        <li class="pure-menu-item">
                            <a href="consult_formation.php" class="pure-menu-link">Consultation



                                <?php if ($profil == 4) {

                                    echo ' et édition';

                                }
                                ?>
                                des  formations</a>

                        </li>


                        <?php if ($profil == 4) {

                            echo '<li class="pure-menu-item"><a href="ajout_formation.php" class="pure-menu-link">Ajouter une formation</a></li>';

                        }
                        ?>

                    </ul>
                </li>

                <!-- Dropdown 2 -->
                <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
                    <a href="#" id="menuLink1" class="pure-menu-link">Demandes de formations</a>

                    <ul class="pure-menu-children">
                        <li class="pure-menu-item"><a href="consult_demande_formation.php" class="pure-menu-link">Consultation des demandes de formations</a></li>


                        <?php if ($profil == 4) {

                            echo '
    <li class="pure-menu-item"><a href="ajout_demande_formation.php" class="pure-menu-link">Ajouter une demande de formation</a></li>';

                        }
                        ?>


                    </ul>
                </li>
                <!-- Dropdown 3 -->


            </ul>



        </nav>

    </header>
</div>


<div class="contenu_page">