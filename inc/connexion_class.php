<?php



class ConnexionModel
{

	private $DB_host;
	private $DB_user;
	private $DB_pass;
	private $DB_name;


	public function __construct(){




		$this->DB_host = "mysql:host=localhost";
		$this->DB_user = "root";
		$this->DB_pass = "";
		$this->DB_name = "stage_dev";

	}

	public function connex_db(){
		// la fonction qui permet de se connecter en pdo au serveur
		try
		{
			$DB_con = new PDO("{$this->DB_host};dbname={$this->DB_name}",$this->DB_user,$this->DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $DB_con;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}







}