<?php

//debug
error_reporting(E_ALL);
ini_set("display_errors", 1);


if (!isset($_SESSION['user'])) {
    session_start();
}

// ON RECUPERE LA CLASSE DE CONNEXION
require_once ('connexion_class.php');
// EVITE DES ERREURS D'AFFICHAGE EN ENVOI DE FORMULAIRE
ob_start();
// ON CHARGE LA CLASSE ConnexionModel en mode dev
$modelconnexion = new ConnexionModel();
// on établi une connexion au serveur attribué à la variable $la_connexion

$la_connexion = $modelconnexion->connex_db();


/**
 *au besoin de se connecter dans la bdd depuis une fonction
 */

function connexionbdd(){

    // ON RECUPERE LA CLASSE DE CONNEXION
    require_once ('connexion_class.php');
    // EVITE DES ERREURS D'AFFICHAGE EN ENVOI DE FORMULAIRE
    ob_start();
    // ON CHARGE LA CLASSE ConnexionModel en mode dev
    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $connex = $modelconnexion->connex_db();

    return $connex;


}



/**
 * Fonction pour afficher des messages flash
 */


function flash($message) {


    if (!isset($_SESSION['erreur'])) {

        // si la variable de session 'nest pas définie'
        $_SESSION['erreur'] = $message;

    }
    if (isset( $_SESSION['erreur'])) {

        echo '<div class="erreur">'.$_SESSION['erreur'].'</div>';
        // si la variable à été définie, on la supprime après affichage du contenu
        unset($_SESSION['erreur']);
    }
}

/* 
// renvoi une liste
    @param : 
        - $profil = vérification des droits du profil de l'agent connecté
        - $méthode = permet d'afficher une liste en fonction d'un choix : par code ou par agent
*/
function get_formation($profil, $methode){


    $la_connexion = connexionbdd();
    switch ($profil) {
        // si l'utilisateur est de la direction, il peut voir TOUTES les formations
        case 1:
            # code...
            break;
        // si l'utilisateur est chef de service, il peut voir toutes les demandes de formations des personnes de son service
        case 2:
            # code...
            break;
        // si l'utilisateur est agent, il peut voir toutes ses propres demandes de formations
        case 3:



            break;
        // si l'utilisateur est administrateur, il peut tout voir et tout modifier
        case 4:
            break;
        default:
            echo "string";
            break;
    }

    $query = 'SELECT id,nom FROM agent WHERE id=? LIMIT 1;';
    $prep = $la_connexion->prepare($query);
    $prep->execute(array($idagent));

    $arrAll = $prep->fetch(PDO::FETCH_ASSOC);

    return $arrAll['nom'];


}



// retourne le nom de l'agent connecté
function returnagent($idagent){

    $la_connexion = connexionbdd();

    $query = 'SELECT * FROM agent WHERE id=? LIMIT 1;';
    $prep = $la_connexion->prepare($query);
    $prep->execute(array($idagent));

    $arrAll = $prep->fetch(PDO::FETCH_ASSOC);

    return $arrAll;

}


// retourne le nom de l'agent connecté
function nomagent($idagent){

    $la_connexion = connexionbdd();

    $query = 'SELECT id,nom FROM agent WHERE id=? LIMIT 1;';
    $prep = $la_connexion->prepare($query);
    $prep->execute(array($idagent));

    $arrAll = $prep->fetch(PDO::FETCH_ASSOC);

    return $arrAll['nom'];

}
// retourne le prénom de l'agent connecté
function prenomagent($idagent){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT id,prenom FROM agent WHERE id=? LIMIT 1;';
    $prep = $la_connexion->prepare($query);
    $prep->execute(array($idagent));

    $arrAll = $prep->fetch(PDO::FETCH_ASSOC);

    return $arrAll['prenom'];

}

// retourne le profil de l'agent connecté
function profilagent($idagent){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT id,profil FROM agent WHERE id=? LIMIT 1;';
    $prep = $la_connexion->prepare($query);
    $prep->execute(array($idagent));

    $arrAll = $prep->fetch(PDO::FETCH_ASSOC);

    return $arrAll['profil'];

}




// retourne un array en prenant le nom de la table en paramètre
function connect_table($nom_table){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table .' ;';

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll(PDO::FETCH_ASSOC);

    return $arrAll;

}

// retourne un array en prenant le nom de la table en paramètre
function connect_table_where($nom_table,$where,$condition){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table . ' WHERE ' . $where . " = '" . $condition . "';" ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll();

    return $arrAll;

}


// retourne un array en prenant le nom de la table en paramètre
function connect_table_where_sans_fetch($nom_table,$where,$condition){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table . ' WHERE ' . $where . ' = ' . $condition . ';' ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep;

    return $arrAll;

}



// retourne un array en prenant le nom de la table en paramètre
function connect_table_where_order_by($nom_table,$where,$condition,$order){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table . ' WHERE ' . $where . ' = ' . $condition . ' ORDER BY ' . $order . ';' ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll();

    return $arrAll;

}

// retourne un array en prenant le nom de la table en paramètre
function connect_table_rq_orderby($rq,$order){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = $rq . ' ORDER BY ' . $order . ';' ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll();

    return $arrAll;

}



// retourne un array en prenant le nom de la table en paramètre
function connect_table_order_by($nom_table,$order){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table . ' ORDER BY ' . $order . ';' ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll();

    return $arrAll;

}


function connect_table_sans_fetch($nom_table){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table .';' ;

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    return $prep;
}

// retourne un array en prenant le nom de la table en paramètre
// $order est un paramètre de la fonction qui permet le order by
function connect_table_sans_assoc($nom_table, $order = 'id'){

    $modelconnexion = new ConnexionModel();
    // on établi une connexion au serveur attribué à la variable $la_connexion
    $la_connexion = $modelconnexion->connex_db();



    $query = 'SELECT * FROM ' . $nom_table .' ORDER BY ' . $order. ' ASC;';

    $prep = $la_connexion->prepare($query);
    $prep->execute();

    $arrAll = $prep->fetchAll();

    return $arrAll;

}

function sort_date_fr($date_bdd){
    $explosion = explode('-', $date_bdd);
    if(isset($explosion[2]) &&  $explosion[2] != '')
    {
        $jour = $explosion[2];
    }
    else {
        $jour = '_';
    }
    if(isset($explosion[1]) && $explosion[1] != '')
    {
        $mois = $explosion[1];
    }
    else {
        $mois = '_';
    }
    if(isset($explosion[0]) &&  $explosion[0] != '')
    {
        $annee = $explosion[0];
    }
    else {
        $annee = '_';
    }

    $retour =  $jour. ' / ' .  $mois . ' / ' .  $annee;
    return $retour;
}

function sort_date_eng_eng($date_to_put){
    $explosion = explode('-', $date_to_put);
    if(isset($explosion[2]) &&  $explosion[2] != '')
    {
        $annee = $explosion[2];
    }
    else {
        $annee = '_';
    }
    if(isset($explosion[1]) && $explosion[1] != '')
    {
        $mois = $explosion[1];
    }
    else {
        $mois = '_';
    }
    if(isset($explosion[0]) &&  $explosion[0] != '')
    {
        $jour = $explosion[0];
    }
    else {
        $jour = '_';
    }

    $retour =  $annee. '-' .  $mois . '-' .  $jour;
    return $retour;
}


// FONCTION pour vérifier si 2 mots de passes hachés sont égaux, adaptation à la version de php
// inutilisée pour des soucis de compatibilité de php 5.3.6
if(!function_exists('hash_equals'))
{
    function hash_equals($str1, $str2)
    {
        if(strlen($str1) != strlen($str2))
        {
            return false;
        }
        else
        {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--)
            {
                $ret |= ord($res[$i]);
            }
            return !$ret;
        }
    }
}

?>