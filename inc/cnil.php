<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 01/06/2016
 * Time: 10:57
 */

?>


<div class="ctn-modele-contenu col-xs-12 col-sm-offset-2 col-sm-8 clearfix" id="text-copy-button-contenu">
    <h3 style="font-size: 28px;">Données personnelles </h3><p style="font-size: 16px;">Les informations recueillies à partir de ce formulaire font l’objet d’un traitement informatique destiné&nbsp;à :</p>
    <p style="font-size: 16px;"><strong>Intranet DGFiP DISI</strong></p>
    <p style="font-size: 16px;">Pour la ou les finalité(s) suivante(s) :&nbsp;</p>
    <p style="font-size: 16px;"><strong>Recensement des demandes de formation</strong></p>
    <p style="font-size: 16px;">Le ou les&nbsp;destinataire(s) des données sont :</p>
    <p style="font-size: 16px;"><strong></strong></p>
    <p style="font-size: 16px;">Conformément à la <a href="/loi-78-17-du-6-janvier-1978-modifiee">loi « informatique et libertés » du 6 janvier 1978 modifiée</a>, vous disposez d’un&nbsp;<a href="/le-droit-dacces">droit d’accès</a> et <a href="/le-droit-de-rectification">de rectification</a> aux informations qui vous concernent.&nbsp;</p>
    <p style="font-size: 16px;">Vous pouvez accèder aux informations vous concernant&nbsp;en vous adressant à&nbsp;:</p>
    <p class="rtecenter" style="font-size: 16px;"><strong>DISI</strong><br>
        &nbsp;</p>
    <p style="font-size: 16px;">Vous pouvez également, pour des motifs légitimes, <a href="/le-droit-dopposition">vous opposer&nbsp;au traitement des données vous concernant</a>.</p>
    <p style="font-size: 16px;">Pour en savoir plus, <a href="/comprendre-vos-droits">consultez&nbsp;vos droits sur le site de la CNIL</a>.</p>
</div>
