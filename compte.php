<?php

// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Compte";
//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';




if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}


$profil = profilagent($_SESSION['user']);




?>


<p>Bienvenue sur votre compte agent <b><?php echo ucfirst(nomagent($_SESSION['user'])) . ' '  . ucfirst(prenomagent($_SESSION['user'])); ; ?></b></p>


<style> .custom-restricted-width { /* To limit the menu width to the content of the menu: */ display: inline-block; /* Or set the width explicitly: */ /* width: 10em; */ } </style>

<div class="pure-menu custom-restricted-width">
	<span class="pure-menu-heading">Formations</span>
	<ul class="pure-menu-list">
		<li class="pure-menu-item"><a href="consult_formation.php" class="pure-menu-link">Consultation des formations</a></li>

		<?php if ($profil == 4) {

			echo '<li class="pure-menu-item"><a href="ajout_formation.php" class="pure-menu-link">Ajouter une formation</a></li>';

		}
		?>

		<li class="pure-menu-heading">Demandes de formations</li>
		<li class="pure-menu-item"><a href="consult_demande_formation.php" class="pure-menu-link">Consultation des demandes de formations</a></li>

		<?php if ($profil == 4) {

			echo '
 		<li class="pure-menu-item"><a href="ajout_demande_formation.php" class="pure-menu-link">Ajouter une demande de formation</a></li>';

		}
		?>


		<li class="pure-menu-heading">Votre compte</li>
		<li class="pure-menu-item"><a href="modif_mdp.php" class="pure-menu-link">Modifier votre mot de passe</a></li>

	</ul>
	</ul>
</div>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>
