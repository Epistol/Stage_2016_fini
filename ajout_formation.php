<?php



// ON MET UN NOM A NOTRE PAGE QUI EST DYNAMIQUEMENT INSERE DANS LE HEADER
$varpage = "Ajouter une formation";


//NOS PETITES FONCTIONS
require_once 'inc/config.php';

// LE HEADER
require_once 'inc/header.php';

// si l'utilisateur n'est 
if (!isset($_SESSION['user'])) {
	header('Location: login.php');
}


// on stocke le profil de l'utilisateur dans la variable $profil
$profil = profilagent($_SESSION['user']);

// si on est pas sur un profil administrateur, on redirige la personne sur son compte
if ($profil != 4) {
	header('Location: compte.php');

}



$formation = connect_table_sans_assoc('formation');
$nom_fournisseur = connect_table('fournisseur');

// si on a envoyé le formulaire
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$codeformation = htmlspecialchars($_POST['code_forma']);
	$nomforma = htmlspecialchars($_POST['nom_forma']);

	$datedebut = sort_date_eng_eng($_POST['datedebut']);

	$datefin = $_POST['datefin'];



	// on récupère chaque champ, on échappe les caractères spéciaux avec htmlspecialchars (éviter d'avoir SELECT * dans un <input> par exemple)
	if (isset($_POST['adresse'])) {
		$adresse = htmlspecialchars($_POST['adresse']);
	}
	if (isset($_POST['zone'])) {
		$zone = htmlspecialchars($_POST['zone']);
	}
	if (isset($_POST['cp'])) {
		$cp = htmlspecialchars($_POST['cp']);
	}
	if (isset($_POST['fournisseur'])) {
		$fournisseur = htmlspecialchars($_POST['fournisseur']);

	}
		if (isset($_POST['periode'])) {
		$periode = htmlspecialchars($_POST['periode']);

	}

	// si les deux champs obligatoires sont bien saisis : (vérification simple)
	if (isset($codeformation) && isset($nomforma)) {

		$stmt = $la_connexion->prepare("INSERT INTO formation (type_formation, nom_formation, date_debut, date_fin, adresse, code_postal, ville, id_fournisseur, periode) 
				VALUES (:type_formation, :nom_formation, :datedebut, :datefin, :adresse, :cp, :ville, :id_fournisseur, :periode)");

		$stmt->bindParam(':type_formation', $codeformation);
		$stmt->bindParam(':nom_formation', $nomforma);
		$stmt->bindParam(':datedebut', $datedebut);
		$stmt->bindParam(':datefin', $datefin);
		$stmt->bindParam(':adresse', $adresse);
		$stmt->bindParam(':cp', $cp);
		$stmt->bindParam(':ville', $zone);
		$stmt->bindParam(':id_fournisseur', $fournisseur);
		$stmt->bindParam(':periode', $periode);
		$stmt->execute();

		echo "L'entrée à bien été ajoutée";

	}

}






?>




	<h2><?php

		// le formulaire :

		// on affiche le nom de la page (flemme)

		echo $varpage; ?></h2>




	<form class="pure-form pure-form-aligned" method="post">
		<fieldset>
			<div class="pure-control-group">
				<label for="code_forma">Code formation</label>
				<input id="code_forma" name="code_forma" type="text" placeholder="" required>
			</div>
			<div class="pure-control-group">
				<label for="nom_forma">Nom de la formation</label>
				<input id="nom_forma" name="nom_forma"type="text" placeholder="" required>
			</div>
			<div class="pure-control-group">
				<label for="datedebut">Date de début : </label>
				<input name="datedebut" id="datedebut" class="dateTxt" type="text" >
			</div>

			<div class="pure-control-group">
				<label for="datefin">Date de fin : </label>
				<input id="datefin" name="datefin" class="dateTxt" type="text" >
			</div>
			<div class="pure-control-group">
				<label for="adresse">Adresse</label>
				<input id="adresse" name="adresse" type="text" placeholder="">
			</div>
			<div class="pure-control-group">
				<label for="zone">Ville ou Zone géographique</label>
				<input id="zone" name="zone" type="text" placeholder="">
			</div>
			<div class="pure-control-group">
				<label for="cp">Code Postal</label>
				<input id="cp" name="cp" type="text" placeholder="ex : 75000">
			</div>
			<div class="pure-control-group">
				<label for="fournisseur">Fournisseur</label>



				<select name="fournisseur" id="fournisseur">
					<?php


					foreach ($nom_fournisseur as $row){
						echo '<option value="' . $row['id'] . '">' . ucfirst($row['lib']) . '</option>';

					} ?>
					<option value="other">Autre</option>
				</select>

			</div>

			<div class="pure-control-group">
				<label for="periode">Période</label>
				<input id="periode" name="periode" type="text" placeholder="si date inconnue">
			</div>


			<div class="pure-controls">

				<button type="submit" name="submit" class="pure-button pure-button-primary">Submit</button>
			</div>
		</fieldset>
	</form>


<?php

// LE PIED DE PAGE
require_once 'inc/footer.php';
?>